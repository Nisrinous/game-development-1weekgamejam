# Bunnious
CSCE60412 Game Development Fakultas Ilmu Komputer Universitas Indonesia 1-Week Game Jam repository.

## Overview
`"Where bunnies explore their world to gain diamonds and rare carrots"`
> Bunnious is a simple platform game that its goal its to help the bunnies getting all the pink and beige diamonds, 
> also getting the precious carrot that usually accour on the highest surface.

## How to Get
> Download Bunnious on: [itch.io](https://nisrinous.itch.io/bunnious)
> or download its directory: [Bunnious exe](https://gitlab.com/Nisrinous/game-development-1weekgamejam/tree/master/Bunnious%20exe)