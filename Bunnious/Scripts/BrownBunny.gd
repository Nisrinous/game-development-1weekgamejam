extends KinematicBody2D

# Declare member variables here
export (int) var speed = 400
export (int) var GRAVITY = 700
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var key_up_pressed = 0

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if key_up_pressed < 3 and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
		key_up_pressed += 1

func _physics_process(delta):
	if is_on_floor():
		key_up_pressed = 0
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
	