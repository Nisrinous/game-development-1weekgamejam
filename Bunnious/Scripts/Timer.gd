extends Label

# Declare member variables
export (int) var minute = 0
export (int) var second = 59

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.text = str(minute) + ":" + str(second)
	pass

func _Timeout():
    get_tree().change_scene(str("res://Scenes/FailedMission.tscn"))

func _on_Timer_timeout():
	if second > 0:
		second -= 1
		if second == 0 and minute != 0:
			minute -= 1
			second = 59
		if second == 0 and minute == 0:
			minute = 0
			_Timeout()
	
		
	pass # Replace with function body.
