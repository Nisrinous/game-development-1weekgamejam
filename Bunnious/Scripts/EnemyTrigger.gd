extends Area2D

export (String) var sceneName = "Level 1"

func _on_Area_Trigger_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Bunni":
        if current_scene == sceneName:
            get_tree().change_scene(str("res://Scenes/" + "GameOver" + ".tscn"))