extends Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D_body_entered(body):
	if body.get_name() == 'Bunni':
		get_parent().get_parent().diamonds += 1
		queue_free()
		if get_parent().get_parent().carrot == 1 and get_parent().get_parent().diamonds == 7:
			queue_free()
			get_tree().change_scene(str("res://Scenes/WinScreen.tscn"))