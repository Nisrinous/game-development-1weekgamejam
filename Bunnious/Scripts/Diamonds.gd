extends Label

# Declare member variables

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.text = "Diamonds : " + str(get_parent().get_parent().get_parent().diamonds) + "/7" + "\nCarrot : " + str(get_parent().get_parent().get_parent().carrot) + "/1"
