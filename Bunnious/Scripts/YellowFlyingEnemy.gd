extends KinematicBody2D

# Declare member variables
const UP = Vector2(0,-1)
var velocity = Vector2()

export (int) var speed = 300
export (int) var GRAVITY = 350

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

# Called when the node enters the scene tree for the first time.
func _ready():
	animator.play("Fly")
	velocity.x = speed

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if is_on_wall():
		velocity.x *= -1
	velocity.y = move_and_slide(velocity).y
